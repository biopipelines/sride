rule fastqc:
    input:
       lambda wildcards: config["samples"][wildcards.sample]
    output:
        html="qc/untrimmed_{sample}.html",
        zip="qc/untrimmed_{sample}_fastqc.zip"
    params: ""
    wrapper:
        "0.27.0/bio/fastqc"

rule fastqc_trimmed:
    input:
       "trimmed/{sample}-trimmed.fq"
    output:
        html="qc/trimmed_{sample}.html",
        zip="qc/trimmed_{sample}_fastqc.zip"
    params: ""
    wrapper:
        "0.27.0/bio/fastqc"

rule fastq_screen:
    input:
        "trimmed/{sample}-trimmed.fq"
    output:
        png="qc/trimmed_{sample}.fastq_screen.png",
        txt="qc/trimmed_{sample}.fastq_screen.txt",
        html="qc/trimmed_{sample}.fastq_screen.html",
        filtered_fastq="qc/trimmed_{sample}.fastq_screen.filtered.fastq"
    conda:
        "envs/fastq_screen.yaml"
    params:
        params=config.get("rules").get("fastq_screen").get("params"),
        config_file=config.get("rules").get("fastq_screen").get("config_file"),
        prefix=lambda wildcards: wildcards.sample
    threads: pipeline_cpu_count()
    shell:
        "fastq_screen  "
        "{params.params} "
        "--conf {params.config_file} "
        "--threads {threads} "
        "{input[0]} "
        "&& find ./ -name {params.prefix}*_screen.txt -type f -print0 | xargs -0 -I file mv " \
        "file {output.txt} ;"
        "find ./ -name {params.prefix}*_screen.png -type f -print0 | xargs -0 -I file mv " \
        "file {output.png} ;"
        "find ./ -name {params.prefix}*_screen.html -type f -print0 | xargs -0 -I file mv " \
        "file {output.html} ;"
        "find ./ -name {params.prefix}*.tagged_filter.fastq -type f -print0 | xargs -0 -I file mv " \
        "file {output.filtered_fastq} "


rule multiqc:
    input:
        expand("qc/trimmed_{sample}_fastqc.zip", sample=config.get('samples')),
        expand("qc/trimmed_{sample}.fastq_screen.txt", sample=config.get('samples')),
        expand("qc/untrimmed_{sample}_fastqc.zip", sample=config.get('samples')),
        expand("trimmed/{sample}.fastq.gz_trimming_report.txt", sample=config.get('samples'))
    output:
        "qc/multiqc.html"
    params:
        ""  # Optional: extra parameters for multiqc.
    log:
        "logs/multiqc/multiqc.log"
    wrapper:
        "0.23.1/bio/multiqc"
